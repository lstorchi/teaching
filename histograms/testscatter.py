import matplotlib.pyplot
import numpy 

x = numpy.random.randn(100)
y = numpy.random.randn(100)

for i in range(len(x)):
    matplotlib.pyplot.plot(x[i], y[i], \
            color = 'green', marker= ".")

matplotlib.pyplot.title("Scatter")
matplotlib.pyplot.xlabel("X")
matplotlib.pyplot.ylabel("Y")
matplotlib.pyplot.show()
