import matplotlib.pyplot
import numpy 

x = numpy.sort(numpy.random.randn(100))
y = [val*val for val in x]

matplotlib.pyplot.plot(x, y, \
            color = 'green', marker= ".")

matplotlib.pyplot.title("Curve")
matplotlib.pyplot.xlabel("X")
matplotlib.pyplot.ylabel("Y")
matplotlib.pyplot.show()
